#include <stdlib.h>
#include <stdio.h>

#include "semantics.h"

void exit_IKS(int iks)
{
  switch(iks)
  {
    // case IKS_SUCCESS: printf("IKS_SUCCESS \n"); exit(IKS_SUCCESS);

    case IKS_ERROR_UNDECLARED: printf("IKS_ERROR_UNDECLARED \n"); break;
    case IKS_ERROR_DECLARED: printf("IKS_ERROR_DECLARED \n"); break;
    
    case IKS_ERROR_VARIABLE: printf("IKS_ERROR_VARIABLE \n"); break;
    case IKS_ERROR_VECTOR: printf("IKS_ERROR_VECTOR \n"); break;
    case IKS_ERROR_FUNCTION: printf("IKS_ERROR_FUNCTION \n"); break;

    case IKS_ERROR_WRONG_TYPE: printf("IKS_ERROR_WRONG_TYPE \n"); break;
    case IKS_ERROR_STRING_TO_X: printf("IKS_ERROR_STRING_TO_X \n"); break;
    case IKS_ERROR_CHAR_TO_X: printf("IKS_ERROR_CHAR_TO_X \n"); break;

    case IKS_ERROR_MISSING_ARGS: printf("IKS_ERROR_MISSING_ARGS \n"); break;
    case IKS_ERROR_EXCESS_ARGS: printf("IKS_ERROR_EXCESS_ARGS \n"); break;
    case IKS_ERROR_WRONG_TYPE_ARGS: printf("IKS_ERROR_WRONG_TYPE_ARGS \n"); break;

    case IKS_ERROR_WRONG_PAR_INPUT: printf("IKS_ERROR_WRONG_PAR_INPUT \n"); break;
    case IKS_ERROR_WRONG_PAR_OUTPUT: printf("IKS_ERROR_WRONG_PAR_OUTPUT \n"); break;
    case IKS_ERROR_WRONG_PAR_RETURN: printf("IKS_ERROR_WRONG_PAR_RETURN \n"); break;
    default: break;
  }
  
  exit(iks);
}