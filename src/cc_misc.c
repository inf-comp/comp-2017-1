#include <stdlib.h>
#include <string.h>

#include "cc_misc.h"
#include "cc_tree.h"
#include "cc_list.h"
#include "generate_code.h"

extern int line_number;
comp_dict_t *dict;
comp_tree_t *ast;
List scope_stack;
extern comp_tree_t* comp_tree_last;


#define printf null_printf

void null_printf(const char* val, ...){

}

void clear_scopes();

int comp_get_line_number (void)
{
  return line_number;
}

void yyerror (char const *mensagem)
{
  fprintf (stderr, "%s\n", mensagem); //altere para que apareça a linha
}

void main_init (int argc, char **argv)
{
  //implemente esta função com rotinas de inicialização, se necessário

  ast = tree_new();
  list_new(&scope_stack, sizeof(dict), NULL); //to-do: check if ok
}

void main_finalize (void) //to-do: destroy all dicts in scope_stack
{
  fprintf(stdout,
  "xor r0, r0 => r0\n"
  "addI r0, 1024 => r1\n");
  gen_code(comp_tree_last);
  clear_scopes();
}

void comp_print_table (void)
{
  printf("(Symbol) \t [line] \t type \t dataType \t name \n");
  int entries = 0;
  int i;
  for (i = 0; i < dict->size; ++i) {
    if (dict->data[i]) {
      symbol_entry *entry = (symbol_entry*) dict->data[i]->value;
      cc_dict_print_entrada(entry);
      ++entries;
    }
  }

  printf("\nNumber of entries: %d / %d \n", entries, dict->size);
}

void *insert_symbol_table(char* yytext, int type, int line_number) {
  printf("parsed: %s\n", yytext);

  // duplica yytext (removendo aspas simples e duplas),
  char* token = calloc(64, sizeof(char));
  strcat(token, strtok(yytext, "\"\'"));


  symbol_entry *lexema_found;
  // lexema_found = _symbol_declared(token);
  lexema_found = dict_get(dict, token);
  if(lexema_found) {
    free(token);
    return lexema_found;
  }

  // Substitui (ou cria) lexema no dicionário
  symbol_entry* lexema = calloc(1, sizeof(symbol_entry));
  lexema->name = strdup(strtok(yytext, "\"\'"));
  // lexema->type = type;
  lexema->line = line_number;
  lexema->dataType = NO_TYPE_DEFINED; //datatype (char, int, ...)
  lexema->type = NO_TYPE_DEFINED; //variable type (array, function, ...)
  lexema->args = NULL;
  switch(type) {
    case TK_LIT_INT:
      lexema->value._integer = atoi(token);
    break;

    case TK_LIT_FLOAT:
      lexema->value._float = atof(token);
    break;

    case TK_LIT_FALSE:
      lexema->value._boolean = false;
    break;

    case TK_LIT_TRUE:
      lexema->value._boolean = true;
    break;

    case TK_LIT_CHAR:
      lexema->value._char = token[0];
    break;

    case TK_LIT_STRING:
      // duplica o token auxiliar, pois este sera
      // guardado como o valor do lexema
      lexema->value._string = strdup(token);
    case TK_IDENTIFICADOR:
    break;

    default:
      printf("ERRO!!! @ insert_symbol_table\n");
      return NULL;
  }

  printf("inserting %s (%p) to scope [%p]\n", lexema->name, lexema, dict);
  void *item = dict_put(dict, token, lexema);
  free(token);

  comp_print_table();

  return item;
}


symbol_entry* _find_symbol(char* token)
{
  symbol_entry *lexema_found;
  int i;
  for(i=0; i < list_size(&scope_stack); i++)
  {
    comp_dict_t *dict_aux;
    list_at(&scope_stack, i, &dict_aux);
    lexema_found = dict_get(dict_aux, token);
    if(lexema_found) return lexema_found;
  }
  return NULL;
}

symbol_entry* _symbol_declared(char* token)
{
  symbol_entry *lexema_found;
  int i;
  for(i=0; i < list_size(&scope_stack); i++)
  {
    comp_dict_t *dict_aux;
    list_at(&scope_stack, i, &dict_aux);
    lexema_found = dict_get(dict_aux, token);
    if(lexema_found) {
      if(lexema_found->type != NO_TYPE_DEFINED)
        return lexema_found;
    }
  }
  return NULL;
}


symbol_entry* declared(symbol_entry* symbol)
{
  return _symbol_declared(symbol->name);
}

void set_variable_global(symbol_entry* symbol){
	symbol->is_global = 1;
}

void unset_variable_global(symbol_entry* symbol){
  //symbol->is_global = 0;
}

void set_variable_offset(symbol_entry* symbol, unsigned long long offset){
  symbol->offset = offset;
}

void set_variable_vector_sizes(symbol_entry* symbol, int_list* sizes){
  printf("setting %s (%p) dimension_sizes to: {", symbol->name, symbol);
  int_list* aux = sizes;
  while(aux){
    printf("%d", aux->value);
    aux = aux->next;
    if(aux) printf(",");
  }
  printf("}\n");
  symbol_entry* lexema;
  lexema->vector_sizes = sizes;
}

bool set_variable_dataType(symbol_entry* symbol, int dataType)
{
  printf("setting %s (%p) dataType to %d\n", symbol->name, symbol, dataType);
  symbol_entry *lexema;

  lexema = _find_symbol(symbol->name); // searches through the stack
  if(lexema) {
    if(lexema->dataType != NO_TYPE_DEFINED) { // redeclaration
      printf("variable redeclaration!\n");
      return false;
    }
    else lexema->dataType = dataType;
    return true;
  }

  exit_IKS(IKS_ERROR_UNDECLARED);
}


bool set_variable_type(symbol_entry* symbol, int type)
{
  printf("setting %s (%p) type to %d\n", symbol->name, symbol, type);
  symbol_entry *lexema;

  lexema = _find_symbol(symbol->name); // searches through the stack
  if(lexema) {
    if(lexema->type != NO_TYPE_DEFINED) { // redeclaration
      printf("variable redeclaration!\n");
      return false;
    }
    else lexema->type = type;
    return true;
  }

  exit_IKS(IKS_ERROR_UNDECLARED);
}


void checkType(symbol_entry* symbol, int type)
{
  symbol = _symbol_declared(symbol->name);
  // if the symbol type is not what is supposed to be
  // we exit with the symbol type error
  if(symbol->type != type)
  {
    printf("testando acesso de %s (%p) com tipo %d \n", symbol->name, symbol, symbol->type);
    switch(symbol->type)
    {
      case IKS_VARIABLE:
        exit_IKS(IKS_ERROR_VARIABLE);
      case IKS_ARRAY:
        exit_IKS(IKS_ERROR_VECTOR);
      case IKS_FUNCTION:
        exit_IKS(IKS_ERROR_FUNCTION);
    }
  }
}


void cc_dict_etapa_1_print_entrada (char *token, int line)
{
  printf("[line %d] \t\t %s \n", line, token);
}

void cc_dict_etapa_2_print_entrada (Entry_value value, int line, int type)
{
  switch(type) {
    case TK_IDENTIFICADOR:
     printf("[line %d] \t (TK_IDENTIFICADOR)\t: %s \n", line, value._string);
    break;

    case TK_LIT_INT:
      printf("[line %d] \t (TK_LIT_INT)\t\t:\t %d \n", line, value._integer);
    break;

    case TK_LIT_CHAR:
      printf("[line %d] \t (TK_LIT_CHAR)\t\t: %c \n", line, value._char);
    break;

    case TK_LIT_STRING:
      printf("[line %d] \t (TK_LIT_STRING)\t: %s \n", line, value._string);
    break;

    case TK_LIT_FLOAT:
      printf("[line %d] \t (TK_LIT_FLOAT)\t\t: %f \n", line, value._float);
    break;

    default:
      printf("(?)");
  }
}


void cc_dict_print_entrada (symbol_entry* entry)
{
  printf("(%p)\t [line %d] \t %d \t %d \t\t %s \n", entry, entry->line, entry->type, entry->dataType, entry->name);
}

void clear_scopes() {
  while(!list_empty(&scope_stack)){
    comp_dict_t** scope_p;
    scope_p = (comp_dict_t**) list_pop_front(&scope_stack);
    printf("terminating scope %p\n\n", *scope_p);
    if(*scope_p) clear_symbol_table(*scope_p);
    free(scope_p);


    if(!list_empty(&scope_stack))
      list_front(&scope_stack, &dict); // updates current scope
    else dict = NULL;
  }
}

// creates new symbol table
// and adds to global scope stack
comp_dict_t* create_scope()
{
  comp_dict_t* scope = dict_new();
  printf("\n * creating scope [%p]\n", scope);
  list_push_front(&scope_stack, &scope);
  list_front(&scope_stack, &dict); // updates current scope
  return scope;
}

// clears the top-most scope in the scope stack
// and removes it from the stack
void finish_scope()
{
  return;
  printf("\n");
  printf("----------------------------------------\n");
  printf("\t SCOPE %p \n", dict);
  comp_print_table();
  printf("----------------------------------------\n");

  comp_dict_t** scope_p;
  scope_p = (comp_dict_t**) list_pop_front(&scope_stack);
  printf("terminating scope %p\n\n", *scope_p);
  if(*scope_p) clear_symbol_table(*scope_p);
  free(scope_p);


  if(!list_empty(&scope_stack))
    list_front(&scope_stack, &dict); // updates current scope
  else dict = NULL;
}

// clears dict entries
void clear_symbol_table(comp_dict_t* dict)
{
  int i = 0;
  symbol_entry *entry;

  for (i; i < dict->size; i++) {
    while(dict->data[i]) {
      entry = (symbol_entry*) dict->data[i]->value;
      printf("freeing entry %p\n", entry);
      // (variable, array and function identifiers) || strings
      if(entry->type != NO_TYPE_DEFINED ||
         entry->dataType == IKS_STRING) {
        printf("freeing str ptr\n");
        free(entry->value._string);
      }

      if(entry->args != NULL) {
        list_destroy(entry->args);
        printf("\n DESTROY LIST \n");
        printf("list size: %d \n", list_size(entry->args));
        free(entry->args);
      }

      // se existe lista de inteiros essa variavel é um array
      // portanto deleta um a um das dimensoes do vetor
      if(entry->vector_sizes != NULL){
        int_list* aux = entry->vector_sizes;
        while(aux){
          int_list* deleted = aux;
          free(deleted);
          aux = aux->next;
        }
      }

      free(entry->name);
      free(entry);
      dict_remove(dict, dict->data[i]->key);
    }
  }
  dict_free(dict);
}

#define printf printf
