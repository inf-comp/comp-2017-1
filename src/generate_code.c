#include "generate_code.h"
#include "cc_misc.h"
#include "cc_ast.h"
#include <assert.h>

void print_literal(ast_value_t* literal);
unsigned long long get_offset(comp_tree_t* n, int* is_in_global_scope, int* offset_in_reg, int* typesize);

int allocd_regs[64] = {0};
static int label_number = 0;

int alloc_register(){
  int i = 3;
  while(allocd_regs[i] != 0){
    i++;
  }
  allocd_regs[i] = 1;
  return i;
}
void dealloc_register(int r){
  allocd_regs[r] = 0;
}

void gen_bool_op(comp_tree_t* ast, int reg){
  ast_value_t* node = (ast_value_t*)ast->value;
  if(node->type == AST_IDENTIFICADOR){
    // some identifier which is a boolean
  } else if(node->type == AST_LITERAL) {
    // boolean literal true or false
  } else {
    int left_reg = alloc_register();
    int right_reg = alloc_register();
    ast_value_t* left_node = ((ast_value_t*)ast->first->value);
    ast_value_t* right_node = ((ast_value_t*)ast->last->value);
    if(left_node->type == AST_LITERAL){
      printf("loadI ");
      print_literal(left_node);
      printf(" => r%d\n", left_reg);
    } else if (left_node->type == AST_IDENTIFICADOR){
      int is_global = 0;
      int offset_in_reg = 0;
      unsigned long long offset = get_offset(ast->first, &is_global, &offset_in_reg, 0);
      if(is_global){
        printf("loadI %d => r%d\n", offset, left_reg);
        printf("load r%d => r%d\n", left_reg, left_reg);
      } else {
        //printf("load r%d => ")
      }
    } else {
      gen_aritm_op(ast->first, left_reg, left_node->type);
    }

    if(right_node->type == AST_LITERAL){
      printf("loadI ");
      print_literal(right_node);
      printf(" => r%d\n", right_reg);
    } else if (right_node->type == AST_IDENTIFICADOR){
      int is_global = 0;
      int offset_in_reg = 0;
      unsigned long long offset = get_offset(ast->last, &is_global, &offset_in_reg, 0);
      if(is_global){
        printf("loadI %d => r%d\n", offset, right_reg);
        printf("load r%d => r%d\n", right_reg, right_reg);
      } else {
        //printf("load r%d => ")
      }
    } else {
      gen_aritm_op(ast->last, right_reg, right_node->type);
    }

    if(node->type == AST_LOGICO_COMP_LE){
      printf("cmp_LE r%d, r%d => r%d\n", left_reg, right_reg, reg);
    } else if(node->type == AST_LOGICO_COMP_GE){
      printf("cmp_GE r%d, r%d => r%d\n", left_reg, right_reg, reg);
    } else if(node->type == AST_LOGICO_COMP_L){
      printf("cmp_LT r%d, r%d => r%d\n", left_reg, right_reg, reg);
    } else if(node->type == AST_LOGICO_COMP_G){
      printf("cmp_GT r%d, r%d => r%d\n", left_reg, right_reg, reg);
    } else if(node->type == AST_LOGICO_COMP_IGUAL){
      printf("cmp_EQ r%d, r%d => r%d\n", left_reg, right_reg, reg);
    } else if(node->type == AST_LOGICO_COMP_DIF){
      printf("cmp_NE r%d, r%d => r%d\n", left_reg, right_reg, reg);
    }
    dealloc_register(left_reg);
    dealloc_register(right_reg);
  }
}

void gen_aritm_op(comp_tree_t* ast, int reg, int op);

void gen_aritm_op(comp_tree_t* ast, int reg, int op){
  int left_reg = alloc_register();
  int right_reg = alloc_register();
  ast_value_t* left_node = ((ast_value_t*)ast->first->value);
  ast_value_t* right_node = ((ast_value_t*)ast->last->value);

  if(left_node->type == AST_LITERAL){
    printf("loadI ");
    print_literal(left_node);
    printf(" => r%d\n", left_reg);
  } else if (left_node->type == AST_IDENTIFICADOR){
    int is_global = 0;
    int offset_in_reg = 0;
    unsigned long long offset = get_offset(ast->first, &is_global, &offset_in_reg, 0);
    if(is_global){
      printf("loadI %d => r%d\n", offset, left_reg);
      printf("load r%d => r%d\n", left_reg, left_reg);
    } else {
      //printf("load r%d => ")
    }
  } else {
    gen_aritm_op(ast->first, left_reg, left_node->type);
  }

  if(right_node->type == AST_LITERAL){
    printf("loadI ");
    print_literal(right_node);
    printf(" => r%d\n", right_reg);
  } else if (right_node->type == AST_IDENTIFICADOR){
    int is_global = 0;
    int offset_in_reg = 0;
    unsigned long long offset = get_offset(ast->last, &is_global, &offset_in_reg, 0);
    if(is_global){
      printf("loadI %d => r%d\n", offset, right_reg);
      printf("load r%d => r%d\n", right_reg, right_reg);
    } else {
      //printf("load r%d => ")
    }
  } else {
    gen_aritm_op(ast->last, right_reg, right_node->type);
  }


  if(op == AST_ARIM_MULTIPLICACAO){
    printf("mult r%d, r%d => r%d\n", left_reg, right_reg, reg);
  } else if(op == AST_ARIM_SOMA){
    printf("add r%d, r%d => r%d\n", left_reg, right_reg, reg);
  } else if(op == AST_ARIM_SUBTRACAO){
    printf("sub r%d, r%d => r%d\n", left_reg, right_reg, reg);
  } else if(op == AST_ARIM_DIVISAO){
    printf("div r%d, r%d => r%d\n", left_reg, right_reg, reg);
  }
  dealloc_register(left_reg);
  dealloc_register(right_reg);
}

int gen_code(comp_tree_t* ast) {
  if(!ast) return;

  ast_value_t* val = (ast_value_t*)ast->value;
  if(val == 0) {
    // this is the root node, do nothing
    gen_code(ast->first);
  }
  else {
    // normal node
    switch(val->type){
      case AST_PROGRAMA:    { gen_code(ast->first); }break;
      case AST_FUNCAO:      { gen_code(ast->first); }break;
      case AST_ATRIBUICAO:  {
        int res_register = gen_code(ast->first->next);
        int is_global = 0;
        int offset_in_reg = 0;
        int tsize = 0;
        unsigned long long offset = get_offset(ast->first, &is_global, &offset_in_reg, &tsize);
        if(offset_in_reg == -1){
          if(is_global){
            printf("storeAI r%d => r0, %llu\n", res_register, offset);
          } else {
            printf("storeAI r%d => r1, %llu\n", res_register, offset);
          }
        } else {
          // multiply by the type size?
          printf("multI r%d, %d => r%d\n", offset_in_reg, tsize, offset_in_reg);
          printf("addI r%d, %d => r%d\n", offset_in_reg, offset, offset_in_reg);
          if(is_global){
            printf("storeAO r%d => r0, r%d\n", res_register, offset_in_reg);
          } else {
            printf("storeAO r%d => r1, r%d\n", res_register, offset_in_reg);
          }
        }

        if(res_register >= 3) dealloc_register(res_register);
    		if(ast->childnodes == 3){
    			gen_code(ast->last);
    		}
      }break;
      case AST_LITERAL: {
        printf("loadI ");
        print_literal(val);
        printf(" => r2\n");
        return 2;
      }break;
  	  case AST_ARIM_SOMA:{
        int result_register = alloc_register();
        gen_aritm_op(ast, result_register, AST_ARIM_SOMA);
        return result_register;
      }break;
      case AST_ARIM_SUBTRACAO:{
        int result_register = alloc_register();
        gen_aritm_op(ast, result_register, AST_ARIM_SUBTRACAO);
        return result_register;
      }break;
      case AST_ARIM_MULTIPLICACAO:{
        int result_register = alloc_register();
        gen_aritm_op(ast, result_register, AST_ARIM_MULTIPLICACAO);
        return result_register;
      }break;
      case AST_ARIM_DIVISAO:{
        int result_register = alloc_register();
        gen_aritm_op(ast, result_register, AST_ARIM_DIVISAO);
        return result_register;
      }break;
      case AST_IDENTIFICADOR:{
        // load value of the variable in a register
        printf("carrega valor de ident em reg\n");
      }break;
      case AST_VETOR_INDEXADO:{
        printf("teste\n");
      }break;
      case AST_IF_ELSE:{
        //printf("ifelse\n");
        int result_register = alloc_register();
        gen_bool_op(ast->first, result_register);
        printf("cbr r%d -> label%d, label%d\n", result_register, label_number, label_number + 1);
        printf("label%d: ", label_number);
        dealloc_register(result_register);
        int label_number2 = label_number + 1;
        int label_number3 = label_number + 2;
        label_number += 3;

        // aqui codigo true
        gen_code(ast->first->next);
        printf("jumpI -> label%d\n", label_number3);
        printf("label%d: ", label_number2);

        // aqui codigo false
        if(ast->childnodes == 4) gen_code(ast->last->prev);
        printf("label%d: ", label_number3);
        if(ast->childnodes == 4) gen_code(ast->last);
      }break;
      case AST_BLOCO:{
        gen_code(ast->first);
        gen_code(ast->last);
      }break;
      default:{
        printf("default type = %d\n", val->type);
      }
    }
  }
}

void print_literal(ast_value_t* literal) {
  symbol_entry* entry = literal->entry;
  switch(literal->dataType)
  {
    case IKS_INT:   printf("%d", entry->value._integer); break;
    case IKS_FLOAT: printf("%d", (int)entry->value._float); break;
    case IKS_BOOL: {
      if(entry->value._boolean == 0) printf("0");
      else printf("1");
    }break;
  }
}

static unsigned long long get_type_size(int dataType){
  switch(dataType){
    case IKS_INT: return sizeof(int);
    case IKS_FLOAT: return 8;
    case IKS_CHAR: return sizeof(char);
    case IKS_BOOL: return 1;
    // short?
  }
}

unsigned long long get_offset(comp_tree_t* n, int* is_global, int* offset_in_reg, int* typesize) {
  symbol_entry* node = (symbol_entry*)((ast_value_t*)n->value)->entry;
  symbol_entry* symbol = declared(node);
  *is_global = symbol->is_global;
  unsigned long long index = 0;
  unsigned long long tsize = get_type_size(((ast_value_t*)n->value)->dataType);
  if(((ast_value_t*)n->value)->type == AST_VETOR_INDEXADO){
    assert(n->childnodes == 2);

  	if(((ast_value_t*)n->last->value)->type != AST_LITERAL) {
      if(((ast_value_t*)n->last->value)->type == AST_IDENTIFICADOR){
        *offset_in_reg = alloc_register();
        int is_global = 0;
        int offreg = 0;
        unsigned long long offset = get_offset(n->last, &is_global, &offreg, 0);
        if(is_global) {
          printf("loadI %d => r%d\n", offset, *offset_in_reg);
          printf("load r%d => r%d\n", *offset_in_reg, *offset_in_reg);
        } else {
          //printf("load r%d => ")
        }
      } else {
        *offset_in_reg = alloc_register();
        gen_aritm_op(n->last, *offset_in_reg, ((ast_value_t*)n->last->value)->type);
      }
  	} else {
  		symbol_entry* expression_inside_indexing = ((ast_value_t*)n->last->value)->entry;
  		index = expression_inside_indexing->value._integer;
      *offset_in_reg = -1;
  	}
    //printf("%d\n", expression_inside_indexing->value._integer);
  } else {
    *offset_in_reg = -1;
  }
  if(typesize) *typesize = tsize;
  return symbol->offset + (index * tsize);

  //unsigned long long typesize = get_type_size(symbol->dataType);
  /*
  if(symbol->type == IKS_ARRAY){
    return 0; // @temporary
  }
  else if(symbol->type == IKS_VARIABLE){
    return 0;
  }
  */
}
