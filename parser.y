/*
  Coloque aqui o identificador do grupo e dos seus membros
  Arthur Lenz
  Jean Flesch
*/
%{
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "cc_ast.h"
#include "cc_tree.h"
#include "cc_dict.h"
#include "cc_list.h"
#include "cc_misc.h"

#include "semantics.h"

comp_tree_t *root = NULL;
int expected_return;

static int global_var_offset = 0;
static int local_var_offset = 0;

int get_dataType(int dataType){
  return dataType;
}

void* _to_ast_value_t(int type, void* entry, int dataType) {
  ast_value_t* v = (ast_value_t*) calloc(1, sizeof(ast_value_t));
  v->type = type;
  v->entry = entry;
  v->dataType = dataType;
  return v;
}

ast_value_t* getNodeValue(comp_tree_t *ast_node) {
  comp_tree_t* node = (comp_tree_t*) ast_node;
  ast_value_t* value = (ast_value_t*) node->value;
  return value;
}

int print_arg(void *data) {
  printf(" %d", *(int*) data);
  return 1;
}

unsigned long long get_type_size(int dataType){
  switch(dataType){
    case IKS_INT: return sizeof(int);
    case IKS_FLOAT: return 8;
    case IKS_CHAR: return sizeof(char);
    case IKS_BOOL: return 1;
    // short?
  }
}

int infer(int type1, int type2) {
  if(type1 == IKS_INT && type2 == IKS_INT) return IKS_INT;
  if(type1 == IKS_FLOAT && type2 == IKS_FLOAT) return IKS_FLOAT;
  if(type1 == IKS_BOOL && type2 == IKS_BOOL) return IKS_BOOL;

  if(type1 == IKS_FLOAT && type2 == IKS_INT ||
     type1 == IKS_INT && type2 == IKS_FLOAT) return IKS_FLOAT;

  if(type1 == IKS_BOOL && type2 == IKS_INT ||
     type1 == IKS_INT && type2 == IKS_BOOL) return IKS_INT;

  if(type1 == IKS_FLOAT && type2 == IKS_BOOL ||
     type1 == IKS_BOOL && type2 == IKS_FLOAT) return IKS_FLOAT;


  // impossible coersion : string and char
  if(type1 == IKS_CHAR || type2 == IKS_CHAR) exit_IKS(IKS_ERROR_CHAR_TO_X);
  if(type1 == IKS_STRING || type2 == IKS_STRING) exit_IKS(IKS_ERROR_STRING_TO_X);

  return IKS_IMPOSSIBLE_INFERENCE;
}

%}

%define parse.error verbose


%union {
  void *int_list;
  void *valor_simbolico_lexico; // symbol_entry
  void *ast_node; // comp_tree_t
  int dataType;
  void *arg_list; // List (storing only the types so far)
}


/* Declaração dos tokens da linguagem */
%token <dataType>TK_PR_INT
%token <dataType>TK_PR_FLOAT
%token <dataType>TK_PR_BOOL
%token <dataType>TK_PR_CHAR
%token <dataType>TK_PR_STRING

%token TK_PR_IF
%token TK_PR_THEN
%token TK_PR_ELSE
%token TK_PR_WHILE
%token TK_PR_DO
%token TK_PR_INPUT
%token TK_PR_OUTPUT
%token TK_PR_RETURN
%token TK_PR_CONST
%token TK_PR_STATIC
%token TK_PR_FOREACH
%token TK_PR_FOR
%token TK_PR_SWITCH
%token TK_PR_CASE
%token TK_PR_BREAK
%token TK_PR_CONTINUE
%token TK_PR_CLASS
%token TK_PR_PRIVATE
%token TK_PR_PUBLIC
%token TK_PR_PROTECTED
%token TK_OC_LE
%token TK_OC_GE
%token TK_OC_EQ
%token TK_OC_NE
%token TK_OC_AND
%token TK_OC_OR
%token TK_OC_SL
%token TK_OC_SR

%token <valor_simbolico_lexico>TK_LIT_INT
%token <valor_simbolico_lexico>TK_LIT_FLOAT
%token <valor_simbolico_lexico>TK_LIT_FALSE
%token <valor_simbolico_lexico>TK_LIT_TRUE
%token <valor_simbolico_lexico>TK_LIT_CHAR
%token <valor_simbolico_lexico>TK_LIT_STRING
%token <valor_simbolico_lexico>TK_IDENTIFICADOR

%token TOKEN_ERRO

%left TK_OC_OR
%left TK_OC_AND
%left TK_OC_EQ TK_OC_NE
%left '<' '>' TK_OC_LE TK_OC_GE
%left '+' '-'
%left '*' '/'
%left NOT


/*program*/
%type<ast_node> program decl

/*function*/
%type<ast_node> function function_decl function_body function_call
%type<arg_list> arguments_list
%type<dataType> argument

/*command*/
%type<ast_node> command command_block command_list command_not_null

/*control flow*/
%type<ast_node> control_flow conditional loop foreach for while do selection case break return
%type<ast_node> input output

/*attribution*/
%type<ast_node> primitive_type_attibution user_defined_type_attibution
%type<ast_node> attribution

/*expression*/
%type<ast_node> expression expression_list
%type<ast_node> arithmetic_operator logic_operator shift_command

%type<ast_node> literal

// node
%type<dataType> type

%type<int_list> list_integers

%%


root:
  new_scope program {
    if(root == NULL) {
      root = tree_make_node(_to_ast_value_t(AST_PROGRAMA, NULL, IKS_DEFER_INFERENCE));
    }
    if(root && $2) { tree_insert_node(root, $2); }

    finish_scope();
  };

new_scope: { create_scope(); };

/* Regras (e ações) da gramática */
program:
  decl  { // declaracao
    $$ = $1;
  }
  | decl program { // permite multiplas declaracoes
    if($1 && $2) { tree_insert_node($1, $2); }
    else {
      if($1) $$ = $1;
      if($2) $$ = $2;
    }
  }
  ;

decl:
  global_decl ';' { $$ = NULL; }
  | type_decl ';' {  $$ = NULL; }
  | function { $$ = $1; }
  | /* nothing */ { $$ = NULL; }
  ;


/*-------------------------------------------------------*/
/* general definitions */
type:
  TK_PR_INT { $$ = IKS_INT; }
  | TK_PR_FLOAT { $$ = IKS_FLOAT; }
  | TK_PR_CHAR { $$ = IKS_CHAR; }
  | TK_PR_BOOL { $$ = IKS_BOOL; }
  | TK_PR_STRING { $$ = IKS_STRING; }
  | TK_IDENTIFICADOR { $$ = IKS_USER_DEFINED; } // used defined type
  ;

literal:
  TK_LIT_TRUE { if($1) $$ = tree_make_node(_to_ast_value_t(AST_LITERAL, $1, IKS_BOOL)); } //ENTRY
  | TK_LIT_FALSE { if($1) $$ = tree_make_node(_to_ast_value_t(AST_LITERAL, $1, IKS_BOOL)); } //ENTRY
  | TK_LIT_INT { if($1) $$ = tree_make_node(_to_ast_value_t(AST_LITERAL, $1, IKS_INT)); } //ENTRY
  | TK_LIT_FLOAT { if($1) $$ = tree_make_node(_to_ast_value_t(AST_LITERAL, $1, IKS_FLOAT)); } //ENTRY
  | TK_LIT_CHAR { if($1) $$ = tree_make_node(_to_ast_value_t(AST_LITERAL, $1, IKS_CHAR)); } //ENTRY
  | TK_LIT_STRING { if($1) $$ = tree_make_node(_to_ast_value_t(AST_LITERAL, $1, IKS_STRING)); } //ENTRY
  ;

/*-------------------------------------------------------*/
/* global variables declaration */
global_decl:
  type TK_IDENTIFICADOR {
    set_variable_global($2);
    if(!set_variable_dataType($2, $1)) exit_IKS(IKS_ERROR_DECLARED);
    if(!set_variable_type($2, IKS_VARIABLE)) exit_IKS(IKS_ERROR_DECLARED);

    unsigned long long tsize = get_type_size($1);
    set_variable_offset($2, global_var_offset);
    global_var_offset += tsize;
  }
  | TK_PR_STATIC type TK_IDENTIFICADOR {
	  set_variable_global($3);
      if(!set_variable_dataType($3, $2)) exit_IKS(IKS_ERROR_DECLARED);
      if(!set_variable_type($3, IKS_VARIABLE)) exit_IKS(IKS_ERROR_DECLARED);
      unsigned long long tsize = get_type_size($2);
      set_variable_offset($2, global_var_offset);
      global_var_offset += tsize;
    }
  | type TK_IDENTIFICADOR '[' TK_LIT_INT ']' {
	  set_variable_global($2);
      if(!set_variable_dataType($2, $1)) exit_IKS(IKS_ERROR_DECLARED);
      if(!set_variable_type($2, IKS_ARRAY)) exit_IKS(IKS_ERROR_DECLARED);
      unsigned long long tsize = get_type_size($1);
      set_variable_offset($2, global_var_offset);
      global_var_offset += tsize * (unsigned long long)((symbol_entry*)$4)->value._integer;
    }
  | TK_PR_STATIC type TK_IDENTIFICADOR '[' TK_LIT_INT ']' {
	  set_variable_global($3);
      if(!set_variable_dataType($3, $2)) exit_IKS(IKS_ERROR_DECLARED);
      if(!set_variable_type($3, IKS_ARRAY)) exit_IKS(IKS_ERROR_DECLARED);
      unsigned long long tsize = get_type_size($2);
      set_variable_offset($2, global_var_offset);
      global_var_offset += tsize * (unsigned long long)((symbol_entry*)$5)->value._integer;
    }
  | type TK_IDENTIFICADOR '[' list_integers ']' {
	set_variable_global($2);
    int_list* list = (int_list*)$4;
    if(!set_variable_dataType($2, $1)) exit_IKS(IKS_ERROR_DECLARED);
    if(!set_variable_type($2, IKS_ARRAY)) exit_IKS(IKS_ERROR_DECLARED);
    set_variable_vector_sizes($2, $4);

    unsigned long long tsize = get_type_size($1);
    unsigned long long total = 1;
    int i = 0;
    while(list){
      total *= list->value;
      list = list->next;
    }
    set_variable_offset($2, global_var_offset);
    global_var_offset += total * tsize;
  }
  | TK_PR_STATIC type TK_IDENTIFICADOR '[' list_integers ']' {
	set_variable_global($3);
    int_list* list = (int_list*)$5;
    if(!set_variable_dataType($3, $2)) exit_IKS(IKS_ERROR_DECLARED);
    if(!set_variable_type($3, IKS_ARRAY)) exit_IKS(IKS_ERROR_DECLARED);
    set_variable_vector_sizes($3, $5);
    unsigned long long tsize = get_type_size($2);
    unsigned long long total = 1;
    int i = 0;
    while(list){
      total *= list->value;
      list = list->next;
    }
    set_variable_offset($2, global_var_offset);
    global_var_offset += total * tsize;
  }
  ;

list_integers:
  TK_LIT_INT {
    symbol_entry* entry = ((symbol_entry*)$1);
    //printf("\n\n\nLAST VALUE IS: %d\n\n\n", entry->value._integer);
    int_list* first = (int_list*)malloc(sizeof(int_list));
    first->value = entry->value._integer;
    first->next = 0;
    $$ = first;
   }
  | TK_LIT_INT ',' list_integers {
    symbol_entry* entry = ((symbol_entry*)$1);
    //printf("\n\n\nTHE VALUE IS: %d\n\n\n", entry->value._integer);
    int_list* val = (int_list*)malloc(sizeof(int_list));
    val->value = entry->value._integer;
    val->next = $3;
    $$ = val;
  }
  ;

/*-------------------------------------------------------*/
/* declaration of new types (user defined types) */
type_decl:
  TK_PR_CLASS TK_IDENTIFICADOR '[' field_list ']'
  ;

field_list:
  ':' field field_list
  | field field_list //to-do: check if this or enables multiple fields
  | // nothing
  ;

field:
  encapsulation type TK_IDENTIFICADOR
  ;

encapsulation:
  TK_PR_PUBLIC
  | TK_PR_PROTECTED
  | TK_PR_PRIVATE
  ;


/*-------------------------------------------------------*/
/* function declaration and definition */
function:
  function_decl new_scope function_body {

    if($1 && $3) {
      $$ = tree_make_unary_node(_to_ast_value_t(AST_FUNCAO, $1, IKS_DEFER_INFERENCE),
                                $3);
    }
    else {
      if($1){
        $$ = tree_make_node(_to_ast_value_t(AST_FUNCAO, $1, IKS_DEFER_INFERENCE));
      }
    }

    finish_scope();
  }
  ;

function_decl:
  type TK_IDENTIFICADOR '(' arguments_list ')' {
    if(!set_variable_dataType($2, $1)) exit_IKS(IKS_ERROR_DECLARED);
    if(!set_variable_type($2, IKS_FUNCTION)) exit_IKS(IKS_ERROR_DECLARED);

    symbol_entry* symbol = declared($2);
    if(symbol) symbol->args = $4;
    list_for_each(symbol->args, print_arg);

    $$ = $2;

    expected_return = $1; // to be used when checking the return param
  }
  | TK_PR_STATIC type TK_IDENTIFICADOR '(' arguments_list ')' {
      if(!set_variable_dataType($3, $2)) exit_IKS(IKS_ERROR_DECLARED);
      if(!set_variable_type($3, IKS_FUNCTION)) exit_IKS(IKS_ERROR_DECLARED);

      symbol_entry* symbol = declared($3);
      if(symbol) symbol->args = $5;
      list_for_each(symbol->args, print_arg);

      $$ = $3;

      expected_return = $2; // to be used when checking the return param
    }
  | type TK_IDENTIFICADOR '(' ')' {
      if(!set_variable_dataType($2, $1)) exit_IKS(IKS_ERROR_DECLARED);
      if(!set_variable_type($2, IKS_FUNCTION)) exit_IKS(IKS_ERROR_DECLARED);
      $$ = $2;

      expected_return = $1; // to be used when checking the return param
    }
  | TK_PR_STATIC type TK_IDENTIFICADOR '(' ')' {
      if(!set_variable_dataType($3, $2)) exit_IKS(IKS_ERROR_DECLARED);
      if(!set_variable_type($3, IKS_FUNCTION)) exit_IKS(IKS_ERROR_DECLARED);
      $$ = $3;

      expected_return = $2; // to be used when checking the return param
    }
  ;

arguments_list:
  argument {
    List *args = (List*) calloc(1, sizeof(List));
    list_new(args, sizeof(int), free);
    list_push_front(args, &$1);
    $$ = args;
  }
  | argument ',' arguments_list {
    list_push_front($3, &$1);
    $$ = $3;
  }
  ;

argument:
  type TK_IDENTIFICADOR { $$ = $1; }
  | TK_PR_CONST type TK_IDENTIFICADOR { $$ = $2; }
  ;

function_body:
  '{' command_list '}' { $$ = $2; }
  ;

command_block:
  new_scope '{' command_list '}' {
    if($3) {
      $$ = tree_make_unary_node(_to_ast_value_t(AST_BLOCO, NULL, IKS_DEFER_INFERENCE), $3);
    }
    else $$ = tree_make_node(_to_ast_value_t(AST_BLOCO, NULL, IKS_DEFER_INFERENCE));

    finish_scope();
  }
  ;

command_list:
  command ';' { $$ = $1; }
  | command ';' command_list {
      if($1 && $3) {
        tree_insert_node($1, $3);
        $$ = $1;
      }
      else {
        if($1) $$ = $1;
        if($3) $$ = $3;
      }
    }
  | command { $$ = $1; }
  ;

command:
  command_not_null { $$ = $1; }
  | /* nothing */ { $$ = NULL; }
  ;

command_not_null:
  local_variable_decl { $$ = NULL; }
  | attribution { $$ = $1; }
  | control_flow { $$ = $1; }
  | input { $$ = $1; }
  | output { $$ = $1; }
  | return { $$ = $1; }
  | command_block ';' { $$ = $1; }
  | function_call { $$ = $1; }
  | shift_command { $$ = $1; }
  ;

local_variable_decl:
  type TK_IDENTIFICADOR {

    if(!set_variable_dataType($2, $1)) exit_IKS(IKS_ERROR_DECLARED);
    if(!set_variable_type($2, IKS_VARIABLE)) exit_IKS(IKS_ERROR_DECLARED);

	  unsigned long long tsize = get_type_size($1);
    set_variable_offset($2, local_var_offset);
    local_var_offset += tsize;
    unset_variable_global($2);

  } attr
  | TK_PR_STATIC type TK_IDENTIFICADOR {
      if(!set_variable_dataType($3, $2)) exit_IKS(IKS_ERROR_DECLARED);
      if(!set_variable_type($3, IKS_VARIABLE)) exit_IKS(IKS_ERROR_DECLARED);
	  unsigned long long tsize = get_type_size($2);
	  set_variable_offset($3, local_var_offset);
	  local_var_offset += tsize;
    unset_variable_global($3);
    } attr
  | TK_PR_CONST type TK_IDENTIFICADOR {
      if(!set_variable_dataType($3, $2)) exit_IKS(IKS_ERROR_DECLARED);
      if(!set_variable_type($3, IKS_VARIABLE)) exit_IKS(IKS_ERROR_DECLARED);
	  unsigned long long tsize = get_type_size($2);
	  set_variable_offset($3, local_var_offset);
	  local_var_offset += tsize;
    unset_variable_global($3);
    } attr
  | TK_PR_STATIC TK_PR_CONST type TK_IDENTIFICADOR {
      if(!set_variable_dataType($4, $3)) exit_IKS(IKS_ERROR_DECLARED);
      if(!set_variable_type($4, IKS_VARIABLE)) exit_IKS(IKS_ERROR_DECLARED);
	  unsigned long long tsize = get_type_size($3);
	  set_variable_offset($4, local_var_offset);
	  local_var_offset += tsize;
    unset_variable_global($4);
    } attr
  | type TK_IDENTIFICADOR '[' list_integers ']' {
    int_list* list = (int_list*)$4;
    if(!set_variable_dataType($2, $1)) exit_IKS(IKS_ERROR_DECLARED);
    if(!set_variable_type($2, IKS_ARRAY)) exit_IKS(IKS_ERROR_DECLARED);
    set_variable_vector_sizes($2, $4);
    unset_variable_global($2);

	unsigned long long tsize = get_type_size($1);
    unsigned long long total = 1;
    int i = 0;
    while(list){
      total *= list->value;
      list = list->next;
    }
    set_variable_offset($2, local_var_offset);
    local_var_offset += total * tsize;
  }
  ;

attr:
  TK_OC_LE literal
  | TK_OC_LE TK_IDENTIFICADOR
  | // nothing
  ;

attribution:
  primitive_type_attibution { $$ = $1; }
  | user_defined_type_attibution { $$ = $1; }
  ;

primitive_type_attibution:
  TK_IDENTIFICADOR '=' expression {

    if(!declared($1)) { exit_IKS(IKS_ERROR_UNDECLARED); }
    checkType($1, IKS_VARIABLE);

    if($1 && $3) {
      symbol_entry* symbol = declared($1);
      ast_value_t* exp = getNodeValue($3);

      if(infer(symbol->dataType, exp->dataType) == IKS_IMPOSSIBLE_INFERENCE) {
        fprintf(stdout, "var = exp : tipos diferentes\n");
        fprintf(stdout, "var dataType %d \n", symbol->dataType);
        fprintf(stdout, "exp dataType %d \n", exp->dataType);
        if(exp->dataType == IKS_STRING) exit_IKS(IKS_ERROR_STRING_TO_X);
        if(exp->dataType == IKS_CHAR) exit_IKS(IKS_ERROR_CHAR_TO_X);
        exit_IKS(IKS_ERROR_WRONG_TYPE);
      }

      $$ = tree_make_binary_node(_to_ast_value_t(AST_ATRIBUICAO, NULL, symbol->dataType),
                                 tree_make_node(_to_ast_value_t(AST_IDENTIFICADOR, $1, symbol->dataType)), // ENTRY
                                 $3);
    }
    else $$ = NULL;
  }

  | TK_IDENTIFICADOR'[' expression ']' '=' expression {

    if(!declared($1)) { exit_IKS(IKS_ERROR_UNDECLARED); }
    checkType($1, IKS_ARRAY);

    if($1 && $3 && $6) {
      symbol_entry* symbol = declared($1);
      ast_value_t* exp1 = getNodeValue($3);
      ast_value_t* exp2 = getNodeValue($6);

      if(exp1->dataType != IKS_INT) {
        printf("var[exp1] = exp2  : exp1 dataType %d\n", exp1->dataType);
        if(exp1->dataType == IKS_STRING) exit_IKS(IKS_ERROR_STRING_TO_X);
        if(exp1->dataType == IKS_CHAR) exit_IKS(IKS_ERROR_CHAR_TO_X);
        exit_IKS(IKS_ERROR_WRONG_TYPE);
      }

      if(infer(symbol->dataType, exp2->dataType) == IKS_IMPOSSIBLE_INFERENCE) {
        if(exp2->dataType == IKS_STRING) exit_IKS(IKS_ERROR_STRING_TO_X);
        if(exp2->dataType == IKS_CHAR) exit_IKS(IKS_ERROR_CHAR_TO_X);
        exit_IKS(IKS_ERROR_WRONG_TYPE);
      }

      $$ = tree_make_binary_node(_to_ast_value_t(AST_ATRIBUICAO, NULL, symbol->dataType),
                                  tree_make_binary_node(_to_ast_value_t(AST_VETOR_INDEXADO, $1, symbol->dataType),
                                                        tree_make_node(_to_ast_value_t(AST_IDENTIFICADOR, $1, symbol->dataType)),
                                                        $3),
                                  $6);
    }
    else $$ = NULL;
  }
  ;

user_defined_type_attibution:
  TK_IDENTIFICADOR'!'field '=' expression { //to-do: check datatype of user defined type

    if(!declared($1)) { exit_IKS(IKS_ERROR_UNDECLARED); }
    checkType($1, IKS_ARRAY);

    if($1 && $5) {
      symbol_entry* symbol = declared($1);
      $$ = tree_make_binary_node(_to_ast_value_t(AST_ATRIBUICAO, NULL, symbol->dataType),
                                 tree_make_node(_to_ast_value_t(AST_IDENTIFICADOR, $1, symbol->dataType)),
                                 $5); //to-do: apontar p entry de field no dict ENTRY
    }
    else $$ = NULL;
  }
  ;

control_flow:
  conditional { $$ = $1; }
  | loop { $$ = $1; }
  | selection { $$ = $1; }
  ;

conditional:
  TK_PR_IF '(' expression ')' TK_PR_THEN command_not_null {
    if($3 && $6) {
      ast_value_t* exp = getNodeValue($3);
      //if(exp->dataType != IKS_BOOL) printf("IF EXP NOT BOOL!\n");

      $$ = tree_make_binary_node(_to_ast_value_t(AST_IF_ELSE, NULL, exp->dataType),
                                $3,  /*conditional*/
                                $6  /*command, if true*/);
    }
    else $$ = NULL;
  }

  | TK_PR_IF '(' expression ')' TK_PR_THEN command TK_PR_ELSE command_not_null {
    if($3 && $6 && $8) {
      ast_value_t* exp = getNodeValue($3);
      //if(exp->dataType != IKS_BOOL) printf("IF ELSE EXP NOT BOOL!\n");

      $$ = tree_make_ternary_node(_to_ast_value_t(AST_IF_ELSE, NULL, exp->dataType),
                                  $3,  /*conditional*/
                                  $6,  /*command, if true*/
                                  $8 /*command, if false*/);
    }
    else $$ = NULL;
    }

loop:
  for { $$ = $1; }
  | foreach { $$ = $1; }
  | while { $$ = $1; }
  | do { $$ = $1; }
  ;

foreach:
  TK_PR_FOREACH '(' TK_IDENTIFICADOR ':' expression_list ')' command { $$ = NULL; }
  ;

for:
  TK_PR_FOR '(' expression_list ':' expression ':' expression_list ')' command { $$ = NULL; }
  ;

while:
  TK_PR_WHILE '(' expression ')' TK_PR_DO command {
    if($6 && $3) {
      ast_value_t* exp = getNodeValue($3);
      if(exp->dataType != IKS_BOOL) printf("WHILE EXP NOT BOOL!\n");

      $$ = tree_make_binary_node((_to_ast_value_t(AST_WHILE_DO, NULL, exp->dataType)),
                               $3, /*conditional*/
                               $6  /*command*/);
    }
    else $$ = NULL;
  }
  ;

do:
  TK_PR_DO command TK_PR_WHILE '(' expression ')' {
    if($2 && $5) {
      ast_value_t* exp = getNodeValue($5);
      if(exp->dataType != IKS_BOOL) printf("DO WHILE EXP NOT BOOL!\n");

      $$ = tree_make_binary_node(_to_ast_value_t(AST_DO_WHILE, NULL, exp->dataType),
                               $2, /*command*/
                               $5  /*conditional*/);
    }
    else $$ = NULL;
  }
  ;

selection:
  TK_PR_SWITCH '(' expression ')' command { $$ = NULL; }
  ;

input:
  TK_PR_INPUT expression {
    if($2) {
      ast_value_t* exp = getNodeValue($2);
      $$ = tree_make_unary_node(_to_ast_value_t(AST_INPUT, NULL, exp->dataType), $2);
    }
    else $$ = NULL;
  }
  ;

output:
  TK_PR_OUTPUT expression_list {
    if($2) {
      //to-do: check type
      ast_value_t* exp = getNodeValue($2);
      $$ = tree_make_unary_node(_to_ast_value_t(AST_OUTPUT, NULL, exp->dataType), $2);
    }
    else $$ = NULL;
  }
  ;

return:
  TK_PR_RETURN expression {
    if($2) {
      ast_value_t* exp = getNodeValue($2);

      if(exp->dataType == IKS_CHAR || exp->dataType == IKS_STRING) {
        if(exp->dataType != expected_return) exit_IKS(IKS_ERROR_WRONG_TYPE_ARGS);
      } // char and string cannot be inferred to other types
      else {
        int inferred = infer(expected_return, exp->dataType);
      }
      $$ = tree_make_unary_node(_to_ast_value_t(AST_RETURN, NULL, exp->dataType), $2);
    }
    else $$ = NULL;
  }
  ;

case:
  TK_PR_CASE TK_LIT_INT ':' { $$ = NULL; }
  ;

break:
  TK_PR_BREAK ';' { $$ = NULL; }
  ;

function_call:
  TK_IDENTIFICADOR '(' expression_list ')' {
    if($1 && $3) {
      if(!declared($1)) exit_IKS(IKS_ERROR_UNDECLARED);
      checkType($1, IKS_FUNCTION);


      comp_tree_t* exp_list = (comp_tree_t*) $3;

      //checking number of arguments expected
      symbol_entry* symbol = declared($1);
      printf("EXPECTING %d ARGUMENT(S)\n", list_size(symbol->args));
      printf("HAS %d ARGUMENT(S)\n", exp_list->childnodes);

      if(exp_list->childnodes < list_size(symbol->args)) exit_IKS(IKS_ERROR_MISSING_ARGS);
      if(exp_list->childnodes > list_size(symbol->args)) exit_IKS(IKS_ERROR_EXCESS_ARGS);

      //iterate over arguments, to check if they are ok
      comp_tree_t* it = exp_list->first;
      int i;
      for(i=0; i < exp_list->childnodes; i++) {
        ast_value_t* v = (ast_value_t*) it->value;
        int expected_dataType;
        list_at(symbol->args, i, &expected_dataType);
        if(v->dataType != expected_dataType) exit_IKS(IKS_ERROR_WRONG_TYPE_ARGS);
      }

      $$ = tree_make_binary_node(_to_ast_value_t(AST_CHAMADA_DE_FUNCAO, NULL, symbol->dataType),
                                 tree_make_node(_to_ast_value_t(AST_IDENTIFICADOR, $1, symbol->dataType)), // ENTRY
                                 $3);
    }
    else $$ = NULL;
  }

  | TK_IDENTIFICADOR '(' ')' {
      if($1) {
        if(!declared($1)) exit_IKS(IKS_ERROR_UNDECLARED);
        checkType($1, IKS_FUNCTION);

        //checking number of arguments expected
        symbol_entry* symbol = declared($1);
        if(symbol->args) { // shouldn't expect any arguments
          printf("EXPECTING %d ARGUMENT(S)\n", list_size(symbol->args));
          printf("HAS %d ARGUMENT(S)\n", 0);

          exit_IKS(IKS_ERROR_MISSING_ARGS);
        }

        $$ = tree_make_unary_node(_to_ast_value_t(AST_CHAMADA_DE_FUNCAO, NULL, symbol->dataType),
                                  tree_make_node(_to_ast_value_t(AST_IDENTIFICADOR, $1, symbol->dataType))); //ENTRY
      }
    }
  ;

shift_command:
  TK_IDENTIFICADOR TK_OC_SL TK_LIT_INT {
    if($1 && $3) {
      symbol_entry* symbol = (symbol_entry*) $1;
      //to-do: check if id is int?
      $$ = tree_make_binary_node(_to_ast_value_t(AST_SHIFT_LEFT, NULL, symbol->dataType),
                                 tree_make_node(_to_ast_value_t(AST_IDENTIFICADOR, $1, symbol->dataType)),
                                 tree_make_node(_to_ast_value_t(AST_LITERAL, $3, symbol->dataType))); //ENTRY
    }
  }
  | TK_IDENTIFICADOR TK_OC_SR TK_LIT_INT {
      if($1 && $3) {
        symbol_entry* symbol = (symbol_entry*) $1;
        //to-do: check if id is int?
        $$ = tree_make_binary_node(_to_ast_value_t(AST_SHIFT_RIGHT, NULL, symbol->dataType),
                                   tree_make_node(_to_ast_value_t(AST_IDENTIFICADOR, $1, symbol->dataType)),
                                   tree_make_node(_to_ast_value_t(AST_LITERAL, $3, symbol->dataType))); //ENTRY
      }
    }
  ;


/*-------------------------------------------------------*/
/* expressions */
expression:
  TK_IDENTIFICADOR {
    if($1){
      symbol_entry* symbol = declared($1);
      $$ = tree_make_node(_to_ast_value_t(AST_IDENTIFICADOR, $1, symbol->dataType));
    }
  } //ENTRY

  | TK_IDENTIFICADOR '[' expression ']' {
    if($1 && $3) {
      symbol_entry* symbol = declared($1);
      $$ = tree_make_binary_node(_to_ast_value_t(AST_VETOR_INDEXADO, NULL, symbol->dataType),
                                 tree_make_node(_to_ast_value_t(AST_IDENTIFICADOR, $1, IKS_INT)), //to-do: check -> MUST BE INTEGER LITERAL
                                 $3); //ENTRY
    }
    else $$ = NULL;
    }

  | literal { $$ = $1; }

  | '(' expression ')' { $$ = $2; }

  | expression arithmetic_operator expression {
      if($2 && $1 && $3) {
        ast_value_t* exp1 = getNodeValue($1);
        ast_value_t* exp2 = getNodeValue($3);

        int inferred = infer(exp1->dataType, exp2->dataType);
        //printf("INFERRED %d\n", inferred);
        ast_value_t* v = $2;
        v->dataType = inferred;
        $$ = tree_make_binary_node($2, $1, $3);
      }
      else $$ = NULL;
    }

  | expression logic_operator expression {
      if($2 && $1 && $3) {
        ast_value_t* exp1 = getNodeValue($1);
        ast_value_t* exp2 = getNodeValue($3);

        int inferred = infer(exp1->dataType, exp2->dataType);
        //printf("INFERRED %d\n", inferred);
        ast_value_t* v = $2;
        v->dataType = inferred;
        $$ = tree_make_binary_node($2, $1, $3);
      }
      else $$ = NULL;
    }

  | '-' expression {
      if($2) {
        ast_value_t* exp = getNodeValue($2);
        $$ = tree_make_unary_node(_to_ast_value_t(AST_ARIM_INVERSAO, NULL, exp->dataType),
                                                  $2);
      }
    }

  | '!' expression %prec NOT {
    ast_value_t* exp = getNodeValue($2);
    $$ = tree_make_unary_node(_to_ast_value_t(AST_LOGICO_COMP_NEGACAO, NULL, exp->dataType),
                                                               $2);
    }

  | function_call { $$ = $1; }

  | shift_command { $$ = $1; }
  ;

expression_list:
  expression {
    $$ = tree_make_node(NULL);
    tree_insert_node($$, $1);
    // $$ = $1;
  }
  | expression ',' expression_list {
    if($1 && $3) {
      // tree_insert_node($1, $3);
      // $$ = $1;
      tree_insert_node($3, $1);
      $$ = $3;
    }
    else {
      if($1) $$ = $1;
      if($3) $$ = $3;
    }
  }
  ;

arithmetic_operator:
  '+' { $$ =  _to_ast_value_t(AST_ARIM_SOMA, NULL, IKS_DEFER_INFERENCE); }
  | '-' { $$ = _to_ast_value_t(AST_ARIM_SUBTRACAO, NULL, IKS_DEFER_INFERENCE); }
  | '*' { $$ = _to_ast_value_t(AST_ARIM_MULTIPLICACAO, NULL, IKS_DEFER_INFERENCE); }
  | '/' { $$ = _to_ast_value_t(AST_ARIM_DIVISAO, NULL, IKS_DEFER_INFERENCE); }
  | '%' { $$ = NULL; } //to-do: olhar qual define tem que usar
  | '^' { $$ = _to_ast_value_t(AST_ARIM_INVERSAO, NULL, IKS_DEFER_INFERENCE); }
  ;

logic_operator:
  TK_OC_AND { $$ = _to_ast_value_t(AST_LOGICO_E, NULL, IKS_DEFER_INFERENCE); }
  | TK_OC_OR { $$ = _to_ast_value_t(AST_LOGICO_OU, NULL, IKS_DEFER_INFERENCE); }
  | TK_OC_NE { $$ = _to_ast_value_t(AST_LOGICO_COMP_DIF, NULL, IKS_DEFER_INFERENCE); }
  | TK_OC_EQ { $$ = _to_ast_value_t(AST_LOGICO_COMP_IGUAL, NULL, IKS_DEFER_INFERENCE); }
  | TK_OC_LE { $$ = _to_ast_value_t(AST_LOGICO_COMP_LE, NULL, IKS_DEFER_INFERENCE); }
  | TK_OC_GE { $$ = _to_ast_value_t(AST_LOGICO_COMP_GE, NULL, IKS_DEFER_INFERENCE); }
  | '<' { $$ = _to_ast_value_t(AST_LOGICO_COMP_L, NULL, IKS_DEFER_INFERENCE); }
  | '>' { $$ = _to_ast_value_t(AST_LOGICO_COMP_G, NULL, IKS_DEFER_INFERENCE); }
  ;
%%
