#ifndef __MISC_H
#define __MISC_H
#include <stdio.h>
#include <stdbool.h>

#include "parser.h"
#include "cc_dict.h"
#include "semantics.h"
#include "cc_list.h"

extern comp_dict_t *dict;
#define DEBUG 0

#define NO_TYPE_DEFINED -1

typedef union Value{
  int _integer;
  float _float;
  char _char;
  bool _boolean;
  char* _string;
} Entry_value;

typedef struct _int_list{
  int value;
  struct _int_list* next;
} int_list;

// struct para tabela de símbolos
typedef struct _symbol_entry{
  char* name;
  int type; // data_type
  int line;
  Entry_value value;
  int dataType; // declared variable type
  List *args; //int list
  int_list* vector_sizes;
  int is_global;
  unsigned long long offset;
} symbol_entry;

void yyerror (char const *mensagem);

int comp_get_line_number (void);

void main_init (int argc, char **argv);
void main_finalize (void);

void *insert_symbol_table(char* yytext, int type, int line_number);
symbol_entry* _symbol_declared(char* token);
bool set_variable_dataType(symbol_entry* symbol, int dataType);
bool set_variable_type(symbol_entry* symbol, int type);
symbol_entry* declared(symbol_entry* symbol);

void checkType(symbol_entry* symbol, int type);

void cc_dict_etapa_1_print_entrada (char *token, int line);
void cc_dict_etapa_2_print_entrada (Entry_value value, int line, int type);
void cc_dict_print_entrada (symbol_entry* entry);

comp_dict_t* create_scope();
void finish_scope();
void clear_symbol_table(comp_dict_t* dict);

#endif
