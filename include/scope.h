#ifndef __SCOPE_H
#define __SCOPE_H
typedef struct scope_tree {
  int id;
  struct scope_tree *next;
  struct scope_tree *previous;
} scope_tree_t;
#endif
