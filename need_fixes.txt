=========
NEED FIX
=========
- Atribuição com vetores multidimensionais (arrumar gramática)
- Não tem precedência aritmética entre soma e adição (tá fazendo o parse da esquerda pra direita)
- If/Else e While não implementados porque há problemas com as expressões

- Obs: pra conseguir testar algumas coisas, desativei o printf em um #define no misc.c

=========
CTEST LOG
=========
68% tests passed, 24 tests failed out of 76

Total Test time (real) =  37.81 sec

The following tests FAILED:
	340 - e5_avaliacao_06_0 (Failed)
	342 - e5_avaliacao_07_0 (Failed)
	343 - e5_avaliacao_07_1 (Failed)
	345 - e5_avaliacao_08_1 (Failed)
	350 - e5_avaliacao_11_0 (Failed)
	351 - e5_avaliacao_11_1 (Failed)
	352 - e5_avaliacao_12_0 (Failed)
	353 - e5_avaliacao_12_1 (Failed)
	354 - e5_avaliacao_13_0 (Failed)
	355 - e5_avaliacao_13_1 (Failed)
	356 - e5_avaliacao_14_0 (Failed)
	357 - e5_avaliacao_14_1 (Failed)
	358 - e5_avaliacao_15_0 (Failed)
	359 - e5_avaliacao_15_1 (Failed)
	360 - e5_avaliacao_16_0 (Failed)
	361 - e5_avaliacao_16_1 (Failed)
	362 - e5_avaliacao_17_0 (Failed)
	363 - e5_avaliacao_17_1 (Failed)
	394 - e5_valgrind_14_0 (Failed)
	395 - e5_valgrind_14_1 (Failed)
	396 - e5_valgrind_15_0 (Failed)
	397 - e5_valgrind_15_1 (Failed)
	398 - e5_valgrind_16_0 (Failed)
	399 - e5_valgrind_16_1 (Failed)
Errors while running CTest



