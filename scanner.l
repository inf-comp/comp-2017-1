/*
  Coloque aqui o identificador do grupo e dos seus membros
  Contribuitors:
    Arthur Lages Lenz
    Jean Ampos Flesch
*/
%{
#include <string.h>
#include "parser.h" //arquivo automaticamente gerado pelo bison
#include "cc_misc.h"

int line_number = 1;

%}

%x comment_block

%%

  /* keywords */
int       return TK_PR_INT;
float     return TK_PR_FLOAT;
bool      return TK_PR_BOOL;
char      return TK_PR_CHAR;
string    return TK_PR_STRING;
if        return TK_PR_IF;
then      return TK_PR_THEN;
else      return TK_PR_ELSE;
while     return TK_PR_WHILE;
do        return TK_PR_DO;
input     return TK_PR_INPUT;
output    return TK_PR_OUTPUT;
return    return TK_PR_RETURN;
const     return TK_PR_CONST;
static    return TK_PR_STATIC;
foreach   return TK_PR_FOREACH;
for       return TK_PR_FOR;
switch    return TK_PR_SWITCH;
case      return TK_PR_CASE;
break     return TK_PR_BREAK;
continue  return TK_PR_CONTINUE;
class     return TK_PR_CLASS;
private   return TK_PR_PRIVATE;
public    return TK_PR_PUBLIC;
protected return TK_PR_PROTECTED;


  /* special characters */
"<=" return TK_OC_LE;
">=" return TK_OC_GE;
"==" return TK_OC_EQ;
"!=" return TK_OC_NE;
"&&" return TK_OC_AND;
"||" return TK_OC_OR;
">>" return TK_OC_SR;
"<<" return TK_OC_SL;


true {
  yylval.valor_simbolico_lexico = insert_symbol_table(yytext, 
                                                     TK_LIT_TRUE, 
                                                     line_number);
  return TK_LIT_TRUE;
}

false {
  yylval.valor_simbolico_lexico = insert_symbol_table(yytext, 
                                                     TK_LIT_FALSE, 
                                                     line_number);
  return TK_LIT_FALSE;
}

  /* identifier & literals */
[_a-zA-Z]+[_a-zA-Z0-9]* {
  yylval.valor_simbolico_lexico = insert_symbol_table(yytext, 
                                                     TK_IDENTIFICADOR, 
                                                     line_number);
  return TK_IDENTIFICADOR;
}

[0-9]+  {
  yylval.valor_simbolico_lexico = insert_symbol_table(yytext, 
                                                     TK_LIT_INT, 
                                                     line_number);
  return TK_LIT_INT;
}

[0-9]+\.[0-9]+  {
  yylval.valor_simbolico_lexico = insert_symbol_table(yytext, 
                                                     TK_LIT_FLOAT, 
                                                     line_number);
  return TK_LIT_FLOAT;
}


'.?'  {
  yylval.valor_simbolico_lexico = insert_symbol_table(yytext, 
                                                     TK_LIT_CHAR, 
                                                     line_number);
  return TK_LIT_CHAR;
}

\"(\\.|[^\\"])*\" {
  yylval.valor_simbolico_lexico = insert_symbol_table(yytext, 
                                                     TK_LIT_STRING, 
                                                     line_number);
  return TK_LIT_STRING;
}


  /* comment block */
"//".*                {}
"/*"                  BEGIN(comment_block);
<comment_block>"*/"   BEGIN(INITIAL);
<comment_block>.      {}
<comment_block>\n     line_number++;


  /* other */
\n    line_number++;


[,;:()<>=!&$^] return yytext[0];
"["   return yytext[0];
"]"   return yytext[0];
"\\"  return yytext[0];
"{"   return yytext[0];
"}"   return yytext[0];
"+"   return yytext[0];
"-"   return yytext[0];
"*"   return yytext[0];
"/"   return yytext[0];

[ \t\v\f\r] {}

.     return TOKEN_ERRO;

%%
