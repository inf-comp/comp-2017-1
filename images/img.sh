folder=$1
output=$2
main=$3

for f in $folder/*.dot
do
  name=$(basename $f)
  filename="${name%.dot}"
  dot $f -Tpng -o $output/$filename.png
done;

for f in $folder/*.ptg
do
  name=$(basename $f)
  filename="${name%.ptg}"
  $main/main < $f
  dot e3.dot -Tpng -o $output/$filename-main.png
done;